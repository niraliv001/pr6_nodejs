const fs = require('fs');
// writing file using fs
fs.writeFileSync('index.txt', 'This file is written using fs');

// reading file
const read = fs.readFileSync('./notesApp/notes.json');
const file = read.toString();
console.warn(file);

// append file
fs.appendFileSync('index.txt','  appended extra data')

// delete file
// fs.unlinkSync('index.txt')

// read file using streaming
const readstream = fs.createReadStream('index.txt')
readstream.on('data',(data)=>{
    const actualdata = data.toString()
    console.log(actualdata)
})
readstream.on('end',()=>{
console.log("end");
})
    
readstream.on('error',(error)=>{
    console.log(error)
})