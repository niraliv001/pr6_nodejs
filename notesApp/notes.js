const fs = require("fs");
const chalk = require("chalk");

const getNotes = () => {
  return "Your notes..";
};
const addNotes = (title, body) => {
  const notes = loadNotes();
  const duplicateNotes = notes.filter((note) => note.title === title);
  if (duplicateNotes.length === 0) {
    notes.push({ title: title, body: body });
    saveNotes(notes);
    console.log("new note added");
  } else {
    console.log("Note title taken");
  }
};
const removeNotes = (title) => {
  const notes = loadNotes();
  const updatedNotes = notes.filter((note) => note.title !== title);
  if (notes.length > updatedNotes.length) {
    console.log(chalk.green.inverse("Note removed"));
    saveNotes(updatedNotes);
  } else {
    console.log(chalk.red.inverse("Note not found"));
  }
};
const loadNotes = () => {
  try {
    const data = fs.readFileSync("notes.json");
    const myObbj = JSON.parse(data);
    return myObbj;
  } catch (error) {
    return [];
  }
};
const saveNotes = (notes) => {
  const newData = JSON.stringify(notes);
  fs.writeFileSync("notes.json", newData);
};
module.exports = {
  getNotes,
  addNotes,
  removeNotes,
};
